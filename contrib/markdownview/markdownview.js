// from jsa Documentation
// to render markdown


function MarkdownView(params,createDom=true) {
jsa.View.call(this,params,false);
//params
this.url = '';
jsa.CopyParams(this,params);

//internals
this.markdownRenderer = new showdown.Converter({simplifiedAutoLink:true});
this.markdownRenderer.setFlavor('github'); // makes it more compatible with github and also gitlab

if(createDom) {
    this.CreateDom();
}
};
MarkdownView.prototype = Object.create(jsa.View.prototype);

MarkdownView.prototype.CreateDom = function() {
jsa.View.prototype.CreateDom.call(this);

//load mdfile via ajax
let self = this;
let xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        self.OnContentLoaded(this.responseText);
    }
};
xhttp.open("GET", this.url, true);
xhttp.send(); 

return this;
};

MarkdownView.prototype.OnContentLoaded = function(content) {
let htmlContent = this.markdownRenderer.makeHtml(content);
this.GetContainingDom().innerHTML = htmlContent;
//this.SetContent(htmlContent)
};