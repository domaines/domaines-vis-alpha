// METACERT 2 plantuml
// most important: generatePicture: reads metacert models (specify level) and returns a link to the svg
// (c) Andreas Waldvogel 2022
// andreas.waldvogel@ils.uni-stuttgart.de


async function generatePicture(level = 1){
    // document.getElementById("loadingtext").innerHTML = "Visualizing, please wait or watch the console...";

    let plantumlStr = ""
    if (level === 2) plantumlStr = await MetaCertM22PlantUML();
    else plantumlStr = await MetaCertM12PlantUML();
    // UTF8 transport encoding http://ecmanaut.blogspot.com/2006/07/encoding-decoding-utf8-in-javascript.html
    plantumlStr = unescape(encodeURIComponent(plantumlStr));
    // zip it https://github.com/johan/js-deflate
    let plantumlStrZipped = deflate(plantumlStr, 9)
    // encode it with ASCII - exclusively for plantuml
    let plantumlStrZipped64 = encode64(plantumlStrZipped)
    let plantumlImgUrl = "http://www.plantuml.com/plantuml/svg/theme/materia/" + plantumlStrZipped64

    return plantumlImgUrl;
}


async function MetaCertM12PlantUML() {
    // global plantuml: 2 parts beginning and ending 
    visPlantuml = "@startuml MetacertStreet";
    visPlantumlTail = "\n\n"
    addSkin()

    // Select specific subfunction to display
    let SUBFUNCTION_NAME = "Systems"

    // Select classes to display
    let CLASSES_TO_DISPLAY = ["Task", "Signal", "Input", "Output"]

    // let selFunction = await domain.Do(CMD.Get(QRY.Cls('Subfunctions').Sel(QRY.Pth('id').Equ("Systems")).Idx(0)))
    // await domain.Do( CMD.Get(QRY.Qry(selFunction).Cls('Task').Pth('name')) )

    let selFunction = await domain.Do(CMD.Get(QRY.Cls('Subfunctions').Sel(QRY.Pth('id').Equ(SUBFUNCTION_NAME)).Idx(0)))
    console.log("selected Function: ", selFunction, 
      "name: ", serializer.Ser(await domain.Do( CMD.Get(QRY.Qry(selFunction).Pth('name'))) )  ) 


    for (className of CLASSES_TO_DISPLAY){
        console.log("className:",className)
        let names = await domain.Do( CMD.Get(QRY.Qry(selFunction).Cls(className).Pth('name')) );
        console.log(`names of class ${className}`, names)
        for (element of names.v) visAdd(`class "${className} ${element.v}"`)
    }


    // // begin of the idea of looping through the objects, but does not really make sense
    // let objectsDisplay = await domain.Do( CMD.Get(QRY.Qry(selFunction).Cls(className)) );
    // console.log("objectsDisplay: ", objectsDisplay)
    // for (object of objectsDisplay.v) {
    //     console.log("processing object:" ,object)
    
    // }


    // // Iterate over all Classes (GET !'1')
    // for (let i = 0; i < await TextQuery(`GET !'1'@SIZE`); i++) {
    //     let curClass = await TextQuery(`GET !'1':${i}`)
    //     console.log("Processing Class: " + curClass)
        
    //     // Iterate over all Class Instances 
    //     for (let i = 0; i < await TextQuery(`GET !'${curClass}'@SIZE`); i++) {
    //         let curObject = await TextQuery(`GET !'${curClass}':${i}`)
    //         console.log("Processing Object: " + await TextQuery(`GET #'${curObject}'/'identifier'`))

    //         await plantElements(curObject, "object")
    //         await plantAttributes(curObject, "/name", " = ", "/'values'")
    //         await plantContainments(curObject);
    //         await plantAssociations(curObject);
    //     }
        
    // }
    return finishVisPlantumlSum();
}	

async function MetaCertM22PlantUML() {
    // global plantuml: 2 parts beginning and ending, globals are bad, but here they work
    visPlantuml = "@startuml MetacertStreet";
    visPlantumlTail = "\n\n"
    addSkin()



    // // Print the namespace (hard coded should be enough, name 1 is important - interprets it as package of other classes)
    // VisAdd("namespace \"" + await TextQuery(`GET #'0.0'/name`) + " 0.0\" as 1" + " {")


    // // Iterate over all Classes (GET !'1')
    // for (let i = 0; i < await TextQuery(`GET !'1'@SIZE`); i++) {
    //     let curClass = await TextQuery(`GET !'1':${i}`)
    //     console.log("Processing Class: " + curClass)

    //     await plantElements(curClass, "class")
    //     await plantAttributes(curClass, "/identifier", " ", "/name", " <", "/'data_type'", ">")
    //     await plantContainments(curClass, true);
    //     await plantInheritances(curClass); // currently only possible on M2
    //     await plantAssociations(curClass, true);
    // }

    return finishVisPlantumlSum();
}

function addSkin(){
    visAdd("hide empty methods") // we don't use methods, so we don't show their space
    visAdd("skinparam linetype ortho")   // https://forum.plantuml.net/1608/is-it-possible-to-only-use-straight-lines-in-a-class-diagram
    visAdd("set namespaceSeparator ::")
    // graphics are better with predefined themes, plantuml server is good example https://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000
    // VisAdd("skinparam shadowing false") // turn of shadow
    // let borderColor = '#596ca6'
    // VisAdd(`skinparam ClassBorderColor ${borderColor}`)
    // VisAdd("skinparam ClassArrowColor Black")
    // VisAdd(`skinparam ObjectBorderColor ${borderColor}`)
    // VisAdd("skinparam ObjectArrowColor Black")
    // VisAdd("skinparam ClassBackgroundColor White")
    // VisAdd("skinparam ObjectBackgroundColor White")
    // VisAdd("skinparam groupInheritance 2") // play around with ordering of inheritance -- brakes layout - not good
    
}

// Header of plantUML element (class or object) with  type and name

async function plantElements(mcElement, elementName="class"){
    // if class and abstract: replace 'class' by 'abstract'
    let declareElement = elementName;
    if(elementName == 'class' && await textQuery(`GET #'${mcElement}'/'is_abstract'`) === 'TRUE') declareElement = 'abstract'

    visAdd(declareElement + " \"" + await textQuery(`GET #'${mcElement}'/'object_type'`) 
            + " " +  mcElement + " " + await textQuery(`GET #'${mcElement}'/'name'`) 
            + "\" as " + mcElement )
}

// Add all Attributes to plantumlstr, format configurable
// qry1: query for first element, sepSym: separation symbol in plantuml, qry2: see qry1, sepSym2 and qry3 are optional
async function plantAttributes(mcElement, qry1, sepSym1 = " = ", qry2, sepSym2 = '', qry3 = '', sepSym3 = ''){
    visAdd("{", false) // without new line because of plantuml object diagram specifications
    for (let i = 0; i < await textQuery(`GET #'${mcElement}'@ATTRIBUTES@SIZE`); i++) {
                if (!await textQuery(`GET #'${mcElement}'@ATTRIBUTES:0`)) break // temporary bug workaround
                let addString = await textQuery(`GET #'${mcElement}'@ATTRIBUTES:${i}${qry1}`) + sepSym1 + await textQuery(`GET #'${mcElement}'@ATTRIBUTES:${i}${qry2}`)
                // add 3rd query if it exists
                if (qry3) addString += sepSym2 + await textQuery(`GET #'${mcElement}'@ATTRIBUTES:${i}${qry3}`) + sepSym3
             
                visAdd(addString)
            }
    visAdd("}\n")
}

// Iterate over all Containments of Element and put on plantUML tail
// Plantuml parses input line by line - all Classes need to be defined before compositions are drawn. Otherwise we get ugly 
// namespace interpretations of 1.2.0
// showMulti: if multiplicities shall be shown
async function plantContainments(mcElement, showMulti = false) {
    for (let i = 0; i < await textQuery(`GET #'${mcElement}'@CONTAINMENTS@SIZE`); i++) {
        if (!await textQuery(`GET #'${mcElement}'@CONTAINMENTS:0`)) break // temporary bug workaround
        if (showMulti) visAddTail(await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/parent`) + " *-- " + ' "'
            + '0..'
            + await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/'child_multiplicity'`)+ '" '
            + await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/child`)
            + ' : ' + await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/identifier`) + ' ' +await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/name`))
        else visAddTail(await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/parent`) + " *-- "
         + await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/child`)
         + ' : ' + await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/identifier`) + ' ' +await textQuery(`GET #'${mcElement}'@CONTAINMENTS:${i}/name`))
    }
}

// same for Inheritances
async function plantInheritances(mcElement) {
    for (let i = 0; i < await textQuery(`GET #'${mcElement}'@IMPLEMENTERS@SIZE`); i++) {
        if (!await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:0`)) break // temporary bug workaround
        visAddTail(mcElement + " <|-- " + await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:${i}`)
           + ' : ' + await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:${i}/identifier`) + ' ' +await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:${i}/name`)) //todo @IMPLEMENTER is not correct, I want to retrieve something like /inheritances 
    }
}

// same for Associations
async function plantAssociations(mcElement, showMulti = false) {
    for (let i = 0; i < await textQuery(`GET #'${mcElement}'@REFERENCES@SIZE`); i++) {
        if (!await textQuery(`GET #'${mcElement}'@REFERENCES:0`)) break // temporary bug workaround - associations are special - they have to null entries

        // check for associations missing start or end and visualize it
        if (await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`) === '-') 
            {
                console.log(`end_object missing: object' ${mcElement}, association `
                + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}`) + ', start_object: '
                + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) + ', end_object: '
                + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`) 
                )
                visAddTail(await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) + " -[#red,dashed,thickness=16] " + '"Missing EndObject, Assoc: ' 
                    + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}`) +  ', start_object: '
                    + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) + '"'               
                    );
                continue;
            }

        if (await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) === '-') 
            {
                console.log(`start_object missing: object' ${mcElement}, association `
                + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}`) + ', start_object: '
                + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) + ', end_object: '
                + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`) 
                )
                visAddTail( '"Missing StartObject, Assoc: ' + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}`) +  ', end_object: '
                    + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`) + '"' 
                    + " -[#red,dashed,thickness=16] " + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`)           
                    );
                continue;
             }

         // if no error, print association. With multiplicities on M2, without on M1
        if (showMulti) visAddTail(await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) + ' "' 
            + '0..'  
            + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_multiplicity'`) +'"' +" -[dashed] " + ' "' 
            + '0..'   
            + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_multiplicity'`) +'" '            
            + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`)
            + ' : ' + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/identifier`) + ' ' +await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/name`))
            else visAddTail(await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'start_object'`) + " -[dashed] " + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/'end_object'`)
                + ' : ' + await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/identifier`) + ' ' +await textQuery(`GET #'${mcElement}'@REFERENCES:${i}/name`))
    }
}

// finish visPlantumlSum
function finishVisPlantumlSum(){
    visPlantumlTail = [...new Set(visPlantumlTail.split("\n"))].join("\n") // delete duplicate compositions, even if I think, this should be corrected in Backend, see https://www.samanthaming.com/tidbits/43-3-ways-to-remove-array-duplicates/
    visAddTail("@enduml")
    let visPlantumlSum = visPlantuml + visPlantumlTail
    visPlantumlSum = visPlantumlSum.replace(/'(.*?)'/g,'$1') // EOQ serializer puts lots of "'", which plantuml can't handle -> delete them ('nummer' = 11 -> nummer = 11), *? makes it lazy

    console.log("\nvisPlantumlSum: \n" + visPlantumlSum)
    return visPlantumlSum;
}

//Define a function to add strings as new lines to the visPlantuml String
function visAdd(plantline, linebreak = true) {
    if (linebreak === true) visPlantuml += "\n" + plantline
    else visPlantuml += plantline
}

//Define a function to add strings as new lines to the visPlantumlTail String
function visAddTail(plantline) {
visPlantumlTail += "\n" + plantline 
}

//Define a function that takes an EOQ String and returns the result String
async function textQuery(queryStr){
    let desQueryStr = serializer.Des(queryStr)
    let QueryRes = await domain.Do(desQueryStr)
    let SerQueryRes = serializer.Ser(QueryRes)
    return SerQueryRes
}