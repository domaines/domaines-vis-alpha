# Documentation
METACER VIS alpha ...a first read-only visualization for Metacert models, 

A very early demonstrator of the read-only visualization of DOMAINES. Only for demonstration purposes. 

A manual, how to test the demonstrator will be provided soon.

<!-- The version used for the publication at the MODELS22-Companion conference is available at tag [v0.1-MODELS22](https://gitlab.com/domaines/domaines-vis-alpha/-/tags/v0.1-MODELS22). -->

## Description
Coming soon. 

## Installation
Coming soon. 

## Usage
Coming soon. 

## Support
If you get in trouble with testing, just drop me an email at andreas.waldvogel@ils.uni-stuttgart.de

## Roadmap
This is an early read-only visualization. A prototypical graphical editor with write capabilities based on XGEE is planned for the end of the year. 

## Contributing
Feel free to contact is, if you are interested in working on a DSME that is aimed to be qualifiable. 

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT License

## Project status
In early development.
