// DOMAINES 2 plantuml
// most important: domainesModel2PlantumlImgUrl: reads a DOMAINES model (specify level) and returns a link to a plantuml visualization as svg
// (c) University of Stuttgart, Institute of Aircraft Systems, Andreas Waldvogel 2022-23
// andreas.waldvogel@ils.uni-stuttgart.de


async function domainesModel2PlantumlImgUrl(level = 1){
    progressbars[level].SetProgress(5);
    const plantumlStr = level === 1 ? await m1Domaines2Plantuml() : await m2Domaines2Plantuml();
    return plantumlStr2Url(plantumlStr, "materia")
}

// function to encode the generated plantuml string into the required url, theme can be specified
// following this guide: https://plantuml.com/code-javascript-synchronous
function plantumlStr2Url(str, theme = 'materia') {
    // plantuml forces UTF8, these two lines do this, I think this is not relevant anymore
    // UTF8 transport encoding http://ecmanaut.blogspot.com/2006/07/encoding-decoding-utf8-in-javascript.html
    // https://andre.arko.net/2012/09/18/force-encoding-in-javascript/
    // const encoded = encodeURIComponent(str)
    // const unescaped = unescape(encoded)
    const compressed = deflate(str, 9); // zip it https://github.com/johan/js-deflate
    const quasiBase64 = encode64(compressed); // encode it with ASCII - exclusively for plantuml
    const url = "https://www.plantuml.com/plantuml/svg/theme/materia/" + quasiBase64;
    return url
}

async function m1Domaines2Plantuml() {
    // global plantuml: 2 parts beginning and ending 
    visPlantuml = "@startuml MetacertStreet";
    visPlantumlTail = "\n\n";
    addSkin();

    console.log("Starting generation of the visualization of M1. ")

    // retrieve all classes
    await textQuery("UPD (#MOD2) name test l1") // switch to M2
    let domClasses = await textQuery("GET (#M0-1/$+INSTANCES)")
    domClasses = stripParantheses(eoqListToArray(domClasses)) // in this case, dropParans ist still necessary, at other places not anymore DISCUSS
    console.log("These are the M2 classes. Their instances will be visualized:", domClasses)
    
    await textQuery("UPD (#MOD1) name test l1") // switch to M1
    
    const progressRange = calculateProgressRange(domClasses.length);

    for (let domClass of domClasses){
        progressbars[1].SetProgress(progressRange[domClasses.indexOf(domClass)]);

        domClass = addDotZero(domClass); // to discuss: why change IDs from "#M0-1.1" to "#M0.0-1.1" depending on level? DISCUSS
        
        // instances of current class
        let instances = await textQuery(`GET (${domClass}/$+INSTANCES)`)
        instances = eoqListToArray(instances)
        console.log("Instances of", domClass,  ": ", instances)
        
        // iterate over instances of current class
        for (const instance of instances){
            if($DEBUG) console.debug("Object in processing: ", instance)
            await plantElements(instance, "object")
            await plantAttributes(instance, ["/name", "/'values'"], [" = "]) 
            await plantContainments(instance);
            await plantAssociations(instance);
        }
    }

    return finishVisPlantumlSum();
}	

async function m2Domaines2Plantuml() {
    // global plantuml: 2 parts beginning and ending, globals are bad, but here they work
    visPlantuml = "@startuml MetacertStreet";
    visPlantumlTail = "\n\n"
    addSkin()

    console.log("Starting generation of the visualization of M2. ")
    
    await textQuery("UPD (#MOD2) name test l1") // switch to M2
    
    // // Print the namespace (hard coded should be enough, name 1 is important - interprets it as package of other classes) // I think, namespaces are currently not supported in AdaEoq3? At least, it is currently not a thing
    // VisAdd("namespace \"" + await TextQuery(`GET #'0.0'/name`) + " 0.0\" as 1" + " {")
    
    let domClasses = await textQuery("GET (#M0-1/$+INSTANCES)")
    domClasses = eoqListToArray(domClasses)
    domClasses = stripParantheses(domClasses)
    console.log("These are the M2 classes. They will be visualized: ", domClasses)

    const progressRange = calculateProgressRange(domClasses.length);

    // Iterate over all Classes
    for (const domClass of domClasses){
        progressbars[2].SetProgress(progressRange[domClasses.indexOf(domClass)]);
        console.log("Visualizing class: ", domClass)
        await plantElements(domClass, "class")
        await plantAttributes(domClass, ["/identifier", "/name", "/datatype"], [" ", " <", ">"], "/identifier", " ", "/name", " <", "/datatype", ">")
        await plantContainments(domClass, true); // show Multi currently not implemented
        await plantInheritances(domClass); // currently only possible on M2
        await plantAssociations(domClass, true);
    }
    
    return finishVisPlantumlSum();
}

// calculate the range - the individual steps - of progress for the progressbars, very similar to a "range" in other programming languages
function calculateProgressRange(numSteps) {
    const start = 20;
    const end = 75;
    const stepSize = Math.floor((end - start) / (numSteps - 1));
    const progressRange = Array.from({length: numSteps}, (_, i) => start + i * stepSize); // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from#examples
    if($DEBUG) console.debug({progressRange});
    return progressRange;
}

// fix for M2 and M1 have different names for the same thing
// turn this #M0-1.1 into this #M0.0-1.1
function addDotZero(str) {
    return str.replace(/(#M\d)/,"$1.0")
}

// fix for plantUML: doesn't accept # and - in names
function properName(str) {
    return str.replace(/-|#/g, "");
}

// function that takes a string and returns the string, but without brackets, trims return of EOQ
// Turns "['[(#M0-2.0)]']" into [(#M0-2.0)], and  ['[ForceEnt,Apple]'] into [ForceEnt,Apple], and [' 7'] into 7
// new: but for M1 ATTRIBUTE into ATTRIBUTE, Yoda into Yoda
function trimBrackets(str){
    // return str.slice(2, -2); // TODO catch [CNULL], low prio // TODO different on M1 - to discuss
    return str.replace(/^\['(.*)'\]$/, "$1")
}

// takes a string list and parses an array
// [(#M0-2.7),(#M0-2.8)] -> array of (#M0-2.7),(#M0-2.8)
function eoqListToArray(str) {
    // if empty, return empty array (and not [""], what is an array with an element)
    if (str === "[]") return []
    // check format
    if (str.startsWith("[") && str.endsWith("]"))  str = str.slice(1, -1) // (#M0-2.7),(#M0-2.8)
    else console.warn("EOQ list does not start with '[' or end with ']'")
    return str.split(',')
}

// takes an object array and strips the "(...)"  [ "(#M0-1.0)", "(#M0-1.1)"] -> [ "#M0-1.0", "#M0-1.1"]
// nowadays only necessary for GET (#M0-1/$+INSTANCES) - DISCUSS
function stripParantheses(arr){
    return arr.map(str => {
        if (str.startsWith("(") && str.endsWith(")")) return str = str.slice(1, -1)
        console.warn(`Object "${str}" of the array has no parentheses. Has the API changed?`)
    } );
}
    
async function textQuery(textCmd){
    let [resStr, resSer] = await domain.SerRawDo(textCmd,sessionId,"TXT");
    res = serializer.DesCmd(resStr);
    outputResStr = serializer.SerVal(res.a[2]);
    let outputResStrTrimmed = trimBrackets(outputResStr) 
    if($DEBUG) console.debug("in:", textCmd, "outRaw:", outputResStr, "out:", outputResStrTrimmed)
    return outputResStrTrimmed;
}

function addSkin(){
    visAdd("hide empty methods") // we don't use methods, so we don't show their space
    visAdd("skinparam linetype ortho")   // https://forum.plantuml.net/1608/is-it-possible-to-only-use-straight-lines-in-a-class-diagram
    visAdd("set namespaceSeparator ::")
    // graphics are better with predefined themes, plantuml server is good example https://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000
}

// Header of plantUML element (class or object) with  type and name

async function plantElements(domElement, elementType="class"){
    // if class and abstract: replace 'class' by 'abstract'
    if(elementType == 'class' && await textQuery(`GET (${domElement}/'isabstract')`) === '[TRUE]') elementType = 'abstract'
    if($DEBUG) console.debug({elementType}, {mcElement: domElement})
    // abstract " #M0-1.0 [ForceEnt]" as M01.0
    visAdd(elementType + ' " '  // # not allowed at beginning - put a ' ' in front of it
            + domElement + ' ' + await textQuery(`GET (${domElement}/'name')`) 
            + '" as ' + properName(domElement) )
            // früher gabe es noch object_type = "class", mal darüber Gedanken machen
}

// Add all Attributes to plantumlstr, format configurable
// query1: query for first element, separationStr1: separation symbol in plantuml, separationString2 and query3 are optional
// class diagram: (#M0-2.0) [force] <[CNULL]>
// object diagram: [ATTRIBUTE] = []
async function plantAttributes(domElement, queries = ["/identifier", "/name", "/datatype"], separators = [" ", " <", ">"]){
    visAdd("{", false) // without new line because of plantuml object diagram specifications

    let attributes = await textQuery(`GET (${domElement}/$+ATTRIBUTES)`)    // GET (#M0-1.3/$+ATTRIBUTES)
    attributes = eoqListToArray(attributes);
    if($DEBUG) console.debug({attributes});

    for (const attribute of attributes){
        if($DEBUG) console.debug({attribute});
        // compose something like GET (#M0-2.0/name)
        let addString = await textQuery(`GET (${attribute}${queries[0]})`) + separators[0] + await textQuery(`GET (${attribute}${queries[1]})`);
        // add 3rd query if it exists
        if (queries[2]) addString += separators[1] + await textQuery(`GET (${attribute}${queries[2]})`)  + separators[2];
        visAdd(addString)
    }

    visAdd("}\n")
}

// Iterate over all Containments of Element and put on plantUML tail
// Plantuml parses input line by line - all Classes need to be defined before compositions are drawn. Otherwise we get ugly 
// namespace interpretations of 1.2.0
// showMulti: if multiplicities shall be shown, currently not implemented
async function plantContainments(domElement, showMulti = false) {

// maybe rename Containment to Composition

    let containments = await textQuery(`GET (${domElement}/compositions)`) // GET (#M0-1.3/compositions), switch to $+ Generic when available 
    containments = eoqListToArray(containments);
    if($DEBUG) console.debug({containments})

    for (const containment of containments){
        if($DEBUG) console.debug({containment});
        // GET (#M0-5.2/parent)
        // "(#M0-1.4)" *-- "(#M0-1.5)" - quotes are necessary
        visAddTail('"' + properName(await textQuery(`GET (${containment}/parent)`)) + '" *-- "'
         + properName(await textQuery(`GET (${containment}/child)`  )) + '"' )
    }

    // TODO multiplicites
    if (false){
        for (let i = 0; i < await textQuery(`GET #'${domElement}'@CONTAINMENTS@SIZE`); i++) {
            if (!await textQuery(`GET #'${domElement}'@CONTAINMENTS:0`)) break // temporary bug workaround
            if (showMulti) visAddTail(await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/parent`) + " *-- " + ' "'
                + '0..'
                + await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/'child_multiplicity'`)+ '" '
                + await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/child`)
                + ' : ' + await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/identifier`) + ' ' +await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/name`))
            else visAddTail(await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/parent`) + " *-- "
            + await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/child`)
            + ' : ' + await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/identifier`) + ' ' +await textQuery(`GET #'${domElement}'@CONTAINMENTS:${i}/name`))
        }
    }
}

// same for Inheritances
async function plantInheritances(domElement) {

    let inheritances = await textQuery(`GET (${domElement}/inheritances)`)   // GET (#M0-1.0/inheritances)
    inheritances = eoqListToArray(inheritances);
    if($DEBUG) console.debug({inheritances})

    for (const inheritance of inheritances){
        if($DEBUG) console.debug({inheritance});
        // GET (#M0-4.0/super)
        // "(#M0-1.0)" <|-- "(#M0-1.1)"
        visAddTail( '"' + properName( await textQuery(`GET (${inheritance}/super)`)) + '" <|-- "' + properName( await textQuery(`GET (${inheritance}/sub)`) )+ '"' )

    // TODO add identifier and name
    }

    if (false){
    for (let i = 0; i < await textQuery(`GET #'${mcElement}'@IMPLEMENTERS@SIZE`); i++) {
        if (!await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:0`)) break // temporary bug workaround
        visAddTail(mcElement + " <|-- " + await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:${i}`)
           + ' : ' + await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:${i}/identifier`) + ' ' +await textQuery(`GET #'${mcElement}'@IMPLEMENTERS:${i}/name`)) //todo @IMPLEMENTER is not correct, I want to retrieve something like /inheritances 
    }
    }
}

// same for Associations
async function plantAssociations(domElement, showMulti = false) {
    
    let associations = await textQuery(`GET (${domElement}/associations)`)  //GET (#M0-1.0/associations), now is  $+ASSOCIATIONS available. Works different, directly points to the assocated class. To discuss. 
    associations = eoqListToArray(associations);
    if($DEBUG) console.debug({associations})

    for (association in associations){
        if($DEBUG) console.debug({association});

        // "(#M0-1.0)" -[dashed] "(#M0-1.4)"
        visAddTail(  '"'+ properName(await textQuery(`GET (${associations}/starting)`)) + '" -[dashed] "' + properName(await textQuery(`GET (${associations}/ending)`)) + '"')
    }

    // TODO care for multiplicities and wrong associations later

    if (false){
    for (let i = 0; i < await textQuery(`GET #'${domElement}'@REFERENCES@SIZE`); i++) {
        if (!await textQuery(`GET #'${domElement}'@REFERENCES:0`)) break // temporary bug workaround - associations are special - they have to null entries

        // check for associations missing start or end and visualize it
        if (await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`) === '-') 
            {
                console.log(`end_object missing: object' ${domElement}, association `
                + await textQuery(`GET #'${domElement}'@REFERENCES:${i}`) + ', start_object: '
                + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) + ', end_object: '
                + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`) 
                )
                visAddTail(await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) + " -[#red,dashed,thickness=16] " + '"Missing EndObject, Assoc: ' 
                    + await textQuery(`GET #'${domElement}'@REFERENCES:${i}`) +  ', start_object: '
                    + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) + '"'               
                    );
                continue;
            }

        if (await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) === '-') 
            {
                console.log(`start_object missing: object' ${domElement}, association `
                + await textQuery(`GET #'${domElement}'@REFERENCES:${i}`) + ', start_object: '
                + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) + ', end_object: '
                + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`) 
                )
                visAddTail( '"Missing StartObject, Assoc: ' + await textQuery(`GET #'${domElement}'@REFERENCES:${i}`) +  ', end_object: '
                    + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`) + '"' 
                    + " -[#red,dashed,thickness=16] " + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`)           
                    );
                continue;
             }

         // if no error, print association. With multiplicities on M2, without on M1
        if (showMulti) visAddTail(await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) + ' "' 
            + '0..'  
            + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_multiplicity'`) +'"' +" -[dashed] " + ' "' 
            + '0..'   
            + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_multiplicity'`) +'" '            
            + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`)
            + ' : ' + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/identifier`) + ' ' +await textQuery(`GET #'${domElement}'@REFERENCES:${i}/name`))
            else visAddTail(await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'start_object'`) + " -[dashed] " + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/'end_object'`)
                + ' : ' + await textQuery(`GET #'${domElement}'@REFERENCES:${i}/identifier`) + ' ' +await textQuery(`GET #'${domElement}'@REFERENCES:${i}/name`))
    }
    }
}

// finish visPlantumlSum
function finishVisPlantumlSum(){
    visPlantumlTail = [...new Set(visPlantumlTail.split("\n"))].join("\n") // delete duplicate compositions, even if I think, this should be corrected in Backend, see https://www.samanthaming.com/tidbits/43-3-ways-to-remove-array-duplicates/
    visAddTail("@enduml")
    let visPlantumlSum = visPlantuml + visPlantumlTail
    // visPlantumlSum = visPlantumlSum.replace(/'(.*?)'/g,'$1') // EOQ serializer puts lots of "'", which plantuml can't handle -> delete them ('nummer' = 11 -> nummer = 11), *? makes it lazy - seems not be relevant anymore

    console.log("\nvisPlantumlSum: \n" + visPlantumlSum)
    return visPlantumlSum;
}

//Define a function to add strings as new lines to the visPlantuml String
function visAdd(plantline, linebreak = true) {
    if (linebreak === true) visPlantuml += "\n" + plantline
    else visPlantuml += plantline
}

//Define a function to add strings as new lines to the visPlantumlTail String
function visAddTail(plantline) {
visPlantumlTail += "\n" + plantline 
}

// //Define a function that takes an EOQ String and returns the result String
// async function textQuery(queryStr){
//     let desQueryStr = serializer.Des(queryStr)
//     let QueryRes = await domain.Do(desQueryStr)
//     let SerQueryRes = serializer.Ser(QueryRes)
//     return SerQueryRes
// }